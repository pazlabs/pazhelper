//
//  PazUIView.swift
//  PazHelper
//
//  Created by Pantelis Zirinis on 22/05/2018.
//  Copyright © 2018 Pantelis Zirinis. All rights reserved.
//

import Foundation
import UIKit

public extension UIView {
    public func findFirstResponder() -> UIView? {
        if self.isFirstResponder {
            return self
        }
        for subView in self.subviews {
            if let firstResponder = subView.findFirstResponder() {
                return firstResponder
            }
        }
        
        return nil;
    }
    
    public func findAndResignFirstResponder() -> Bool {
        if let firstResponder = self.findFirstResponder() {
            return firstResponder.resignFirstResponder()
        }
        return false
    }
    
    public func addSubviews(_ subviews: [UIView]) {
        for view in subviews {
            self.addSubview(view)
        }
    }
    
    public func screenshot() -> UIImage {
        let rect = self.bounds
        UIGraphicsBeginImageContextWithOptions(rect.size, true, 0.0)
        if let context = UIGraphicsGetCurrentContext() {
            self.layer.render(in: context)
            let capturedImage = UIGraphicsGetImageFromCurrentImageContext();
            UIGraphicsEndImageContext();
            return capturedImage!;
        } else {
            return UIImage()
        }
    }
    
    #if SAVETOPHOTOALBUM
    // we had to include the compile tag as app store was requesting for photo library access key
    public func saveScreenshotToPhotosAlbum() {
        UIImageWriteToSavedPhotosAlbum(self.screenshot(), nil, nil, nil)
    }
    #endif
    public class func setAlpha(_ alpha: Double, forViews: Array<UIView>) {
        for view in forViews {
            view.alpha = CGFloat(alpha)
        }
    }
    
    public class func setHidden(_ hidden: Bool, forViews: Array<UIView>) {
        for view in forViews {
            view.isHidden = hidden
        }
    }
    
    public var viewController: UIViewController? {
        // convenience function for casting and to "mask" the recursive function
        return self.traverseResponderChainForUIViewController()
    }
    
    func traverseResponderChainForUIViewController() -> UIViewController? {
        if let nextResponder = self.next as? UIViewController {
            return nextResponder
        }
        if let nextResponder = self.next as? UIView {
            return nextResponder.traverseResponderChainForUIViewController()
        }
        return nil
    }
    
    public var xEnd: CGFloat {
        return self.frame.origin.x + self.frame.size.width
    }
    public var yEnd: CGFloat {
        return self.frame.origin.y + self.frame.size.height
    }
    
    public func horizontalCenterOn(_ view: UIView) {
        var frame = self.frame;
        frame.origin.x = (view.frame.size.width - frame.size.width) / 2.0;
        self.frame = frame;
    }
    
    public func verticalCenterOn(_ view: UIView) {
        var frame = self.frame;
        frame.origin.y = (view.frame.size.height - frame.size.height) / 2.0;
        self.frame = frame;
    }
    
    public func centerOn(_ view: UIView) {
        var frame = self.frame;
        frame.origin.x = (view.frame.size.width - frame.size.width) / 2.0;
        frame.origin.y = (view.frame.size.height - frame.size.height) / 2.0;
        self.frame = frame;
    }
}
