//
//  PazString.swift
//  PazHelper
//
//  Created by Pantelis Zirinis on 22/05/2018.
//  Copyright © 2018 Pantelis Zirinis. All rights reserved.
//

import Foundation

public extension String {
    
    /// Returns whether string is a valid url
    public var validUrl: Bool {
        let urlRegEx = "((https|http)://)((\\w|-)+)(([.]|[/])((\\w|-)+))+"
        let predicate = NSPredicate(format:"SELF MATCHES %@", argumentArray:[urlRegEx])
        //let urlTest = NSPredicate.predicateWithSubstitutionVariables(predicate)
        
        return predicate.evaluate(with: self)
    }

    /**
     Strips whitespaces.
     - returns: Stripped string
     */
    func trimmed () -> String {
        return self.trimmingCharacters(in: CharacterSet.whitespaces)
    }
    
    /**
     Parses a string containing a double numerical value into an optional double if the string is a well formed number.
     - returns: A double parsed from the string or nil if it cannot be parsed.
     */
    func toDouble() -> Double? {
        
        let scanner = Scanner(string: self)
        var double: Double = 0
        
        if scanner.scanDouble(&double) {
            return double
        }
        
        return nil
        
    }
    
    /**
     Parses a string containing a float numerical value into an optional float if the string is a well formed number.
     - returns: A float parsed from the string or nil if it cannot be parsed.
     */
    func toFloat() -> Float? {
        
        let scanner = Scanner(string: self)
        var float: Float = 0
        
        if scanner.scanFloat(&float) {
            return float
        }
        
        return nil
        
    }
    
    /**
     Parses a string containing a non-negative integer value into an optional UInt if the string is a well formed number.
     - returns: A UInt parsed from the string or nil if it cannot be parsed.
     */
    func toUInt() -> UInt? {
        if let val = Int(self.trimmed()) {
            if val < 0 {
                return nil
            }
            return UInt(val)
        }
        
        return nil
    }
    
    
    /**
     Parses a string containing a boolean value (true or false) into an optional Bool if the string is a well formed.
     - returns: A Bool parsed from the string or nil if it cannot be parsed as a boolean.
     */
    func toBool() -> Bool? {
        let text = self.trimmed().lowercased()
        if text == "true" || text == "yes" || text == "1" {
            return true
        }
        if text == "false" || text == "no" || text == "0" {
            return false
        }
        
        return nil
    }
    
    /**
     Parses a string containing a date into an optional Date if the string is a well formed.
     The default format is yyyy-MM-dd, but can be overriden.
     - returns: A Date parsed from the string or nil if it cannot be parsed as a date.
     */
    func toDate(_ format : String? = "yyyy-MM-dd") -> Date? {
        let text = self.trimmed().lowercased()
        let dateFmt = DateFormatter()
        dateFmt.timeZone = NSTimeZone.default
        if let fmt = format {
            dateFmt.dateFormat = fmt
        }
        return dateFmt.date(from: text)
    }
    
    /**
     Parses a string containing a date and time into an optional Date if the string is a well formed.
     The default format is yyyy-MM-dd hh-mm-ss, but can be overriden.
     - returns: A Date parsed from the string or nil if it cannot be parsed as a date.
     */
    func toDateTime(_ format : String? = "yyyy-MM-dd hh-mm-ss") -> Date? {
        return toDate(format)
    }
    
    /// Evaluates wheather string is a valid email address
    #if !os(Linux)
    public func isEmail() -> Bool {
        let emailRegEx = "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: self)
    }
    #endif
    func beginsWith (_ str: String) -> Bool {
        if let range = self.range(of: str) {
            return range.lowerBound == self.startIndex
        }
        return false
    }
    
    func endsWith (_ str: String) -> Bool {
        if let range = self.range(of: str, options:String.CompareOptions.backwards) {
            return range.upperBound == self.endIndex
        }
        return false
    }
}
