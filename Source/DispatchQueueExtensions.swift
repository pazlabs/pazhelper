//
//  DispatchQueueExtensions.swift
//  PazHelper
//
//  Created by Pantelis Zirinis on 20/03/2018.
//  Copyright © 2018 Pantelis Zirinis. All rights reserved.
//

import Foundation

public func executeMainWithDelay(_ delay:Double, closure:@escaping ()->()) {
    DispatchQueue.main.asyncAfter(deadline: .now() + delay) {
        closure()
    }
}

public func executeMain(ignoreIfAlreadyInMain: Bool = true, _ closure:@escaping ()->()) {
    if ignoreIfAlreadyInMain {
        if Thread.isMainThread {
            closure()
        } else {
            DispatchQueue.main.async {
                closure()
            }
        }
    } else {
        closure()
    }
}

@available(iOS 8.0, *)
public func executeBackground(_ closure:@escaping ()->()) {
    let backgroundQueue = DispatchQueue.global(qos: DispatchQoS.QoSClass.background)
    backgroundQueue.async {
        closure()
    }
}

@available(iOS 8.0, *)
public func executeBackgroundWithdelay(_ delay:Double, closure:@escaping ()->()) {
    let backgroundQueue = DispatchQueue.global(qos: DispatchQoS.QoSClass.background)
    backgroundQueue.asyncAfter(deadline: .now() + delay) {
        closure()
    }
}

public extension DispatchQueue {
    public typealias DispatchClosure = () -> (Void)
    public func barrierSync(closure: DispatchClosure) {
        return self.sync(flags: DispatchWorkItemFlags.barrier) {
            closure()
        }
    }
}
