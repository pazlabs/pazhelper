//
//  StringConvertions.swift
//  PazHelper
//
//  Created by Pantelis Zirinis on 19/03/2018.
//  Copyright © 2018 Pantelis Zirinis. All rights reserved.
//

import Foundation

public extension Double {
    public func format(_ f: String) -> String {
        return String(format: "%\(f)f", self)
    }
}
